package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

public abstract class WebBaseTest {

    public void setUp(){
        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.driverManagerEnabled = true;
        Configuration.browserSize = "1920x1080";
        Configuration.headless = true;
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-dev-shm-usage");
        Configuration.browserCapabilities = chromeOptions;
    }

    @BeforeClass(alwaysRun = true)
    public void init(){
        setUp();
    }

    @BeforeMethod(alwaysRun = true)
    public void openBrowser(){
        Selenide.open("https://www.airbnb.com/");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
        Selenide.closeWebDriver();
    }
}
