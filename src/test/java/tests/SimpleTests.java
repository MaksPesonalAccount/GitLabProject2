package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.TmsLink;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import testrailupdate.TestNGTestRailListeners;

@Listeners(TestNGTestRailListeners.class)
public class SimpleTests {

    @TmsLink("1")
    @Test
    public void test1() {
        Configuration.headless = true;
        Selenide.open("https://www.airbnb.com/");
        System.out.println(1);
        Assert.assertEquals(5, 5);
    }

    @TmsLink("2")
    @Test
    public void test2() {
        Configuration.headless = true;
        Selenide.open("https://www.airbnb.com/");
        Assert.assertEquals(5, 5);
    }

    @TmsLink("3")
    @Test
    public void test3() {
        Configuration.headless = true;
        Selenide.open("https://www.airbnb.com/");
        Assert.assertEquals(5, 5);
    }

    @TmsLink("4")
    @Test
    public void test4() {
        Assert.assertEquals(5, 5);
    }

    @TmsLink("5")
    @Test
    public void test5() {
        Assert.assertEquals(5, 5);
    }

}
