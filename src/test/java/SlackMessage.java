import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;

public class SlackMessage {

    public static void sendMessage(String token, String channel, String message) throws Exception {
        Slack slack = Slack.getInstance();
        MethodsClient methods = slack.methods(token);
        ChatPostMessageRequest request = ChatPostMessageRequest.builder()
                .channel(channel)
                .text(message)
                .build();
        ChatPostMessageResponse response = methods.chatPostMessage(request);
        if (!response.isOk()) {
            throw new Exception(response.getError());
        }
    }
}
