import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.model.block.LayoutBlock;
import com.slack.api.model.block.SectionBlock;
import com.slack.api.model.block.composition.MarkdownTextObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SlackNotifier {
    private final String SLACK_API_TOKEN = "xoxb-5016425635046-5035840943057-OwFbrRYSzrGHDj5wnnEZW7rw";
    private final String SLACK_CHANNEL = "C0508EUTAS3";

    public void sendMessage(String message) {
        // create a Slack methods client
        MethodsClient client = Slack.getInstance().methods(SLACK_API_TOKEN);

        // create a message request object
        ChatPostMessageRequest request = ChatPostMessageRequest.builder()
                .channel(SLACK_CHANNEL)
                .text(message)
                .build();

        try {
            // send the message to the channel
            client.chatPostMessage(request);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SlackApiException e) {
            throw new RuntimeException(e);
        }
    }

    public void sendMessageWithBlocks(String message, List<LayoutBlock> blocks) {
        // create a Slack methods client
        MethodsClient client = Slack.getInstance().methods(SLACK_API_TOKEN);

        // create a section block with the message text
        SectionBlock sectionBlock = SectionBlock.builder()
                .text(MarkdownTextObject.builder().text(message).build())
                .build();

        // add the section block to the list of blocks
        List<LayoutBlock> allBlocks = new ArrayList<>(blocks);
        allBlocks.add(sectionBlock);

        // create a message request object with the blocks
        ChatPostMessageRequest request = ChatPostMessageRequest.builder()
                .channel(SLACK_CHANNEL)
                .blocks(allBlocks)
                .build();

        try {
            // send the message with blocks to the channel
            client.chatPostMessage(request);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SlackApiException e) {
            throw new RuntimeException(e);
        }
    }
}
