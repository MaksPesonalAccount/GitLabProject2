package features;

import pages.LoginPage;
import pages.MainPage;

public class LoginFeature {
    private LoginPage loginPage;
    private MainPage mainPage;

    public LoginFeature(){
        this.loginPage = new LoginPage();
        this.mainPage = new MainPage();
    }

    public void logInFromMainPage(){
        mainPage.openProfileAndClickLogIn();
        loginPage.loginViaGoogleEmail();
    }
}
