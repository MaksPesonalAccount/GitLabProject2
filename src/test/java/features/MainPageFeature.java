package features;

import pages.LoginPage;
import pages.MainPage;

public class MainPageFeature {

    private LoginPage loginPage;
    private MainPage mainPage;

    public MainPageFeature() {
        this.loginPage = new LoginPage();
        this.mainPage = new MainPage();
    }

    public void openFirstRoomFromList(){
        mainPage.openMainPage().clickOnRooms().openFirstRoom();
    }

}
