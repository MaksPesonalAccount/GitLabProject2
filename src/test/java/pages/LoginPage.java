package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {
    private final SelenideElement logWithEmail = $x("//button[.='Continue with email']");
    private final SelenideElement emailInput = $x("//input[@id='email-login-email']");
    private final SelenideElement passwordInput = $x("//input[@id='email-signup-password']");
    private final SelenideElement continueButton = $x("//button[@data-testid='signup-login-submit-btn']");
    private final SelenideElement logInButton = $x("//button[@data-testid = 'signup-login-submit-btn']");
    public void loginViaGoogleEmail(){
        logWithEmail.click();
        emailInput.setValue("maksimstovbcatij@gmail.com");
        continueButton.click();
        passwordInput.setValue("ZCAlGxtD!");
        logInButton.click();
        System.out.println(1);
    }
}
