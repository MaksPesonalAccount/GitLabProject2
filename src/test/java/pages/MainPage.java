package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage {
    private final String BASE_URL = "https://www.airbnb.com/";
    private final SelenideElement profileIcon = $x("//nav[@aria-label = 'Profile']//div[@class = '_3hmsj']");
    private final SelenideElement navigationMenu = $x("//button[@aria-label='Main navigation menu']");
    private final SelenideElement announcement = $x("//div[@role = 'dialog' and @aria-modal = 'true']");
    private final SelenideElement logInButton = $x(".//div[text() = 'Log in']");
    private final SelenideElement close = $x("//button[@aria-label = 'Close']");
    private final SelenideElement rooms = $x("//span[text() = 'Rooms']");
    private final ElementsCollection listRoomsOnPage = $$x("//div[@class = 'c4mnd7m dir dir-ltr']");

    public MainPage openMainPage(){
        Selenide.open(BASE_URL);
        closeAnnouncement();
        return this;
    }

    public void closeAnnouncement(){
        Selenide.sleep(2000);
        if (announcement.isDisplayed()){
            announcement.shouldBe(Condition.visible);
            close.click();
        }
    }
    public void openProfileAndClickLogIn(){
        openMainPage();
        profileIcon.click();
        navigationMenu.shouldBe(Condition.visible);
        logInButton.click();
    }

    public MainPage clickOnRooms(){
        rooms.click();
        return this;
    }

    public MainPage openFirstRoom(){
        Selenide.sleep(2000);
        listRoomsOnPage.first().click();
        System.out.println(1);
        return this;
    }


}
