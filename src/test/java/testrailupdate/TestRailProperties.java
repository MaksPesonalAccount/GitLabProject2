package testrailupdate;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class TestRailProperties {
    private static final String PROPERTIES_FILE = "src/test/resources/testrail.properties";
    private static Properties properties;

    static {
        try {
            properties = new Properties();
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(PROPERTIES_FILE);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getBaseUrl() {
        return properties.getProperty("baseurl").replaceAll("\"", "");
    }

    public static String getUsername() {
        return properties.getProperty("username").replaceAll("\"", "");
    }

    public static String getPassword() {
        return properties.getProperty("password").replaceAll("\"", "");

    }
}

