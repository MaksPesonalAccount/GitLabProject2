package testrailupdate;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestRailBasicApiCommand {
    private static final String URL = TestRailProperties.getBaseUrl();
    private static final String username = TestRailProperties.getUsername();
    private static final String password = TestRailProperties.getPassword();

    //Test method
    public static void getTitleOfTestCase(int testCaseID){
        APIClient client = new APIClient(URL);
        client.setUser(username);
        client.setPassword(password);
        JSONObject c;
        try {
            c = (JSONObject) client.sendGet("get_case/" + testCaseID);

        } catch (IOException | APIException e) {
            throw new RuntimeException(e);
        }
        System.out.println(c.get("title"));
    }

    // add_result_for_case/3/1 it's end point where 3 is id of test run, and 1 is id of test case
    public static void postResultOfTestCase(int statusID, int runId, int testCaseId, String comment)throws APIException, IOException  {
        APIClient client = new APIClient(URL);
        client.setUser(username);
        client.setPassword(password);
        JSONObject c = null;
        //can be re-written to post result with other details
        Map data = new HashMap();
        data.put("status_id", statusID);
        data.put("comment", comment);
        //run id visible from url on TestRail just sign in to TestRail go to TestRun u will se run id after view
        //https://testrailplayground123.testrail.io/index.php?/runs/view/3&group_by=cases:section_id&group_order=asc&group_id=1 - run_id is 3
        JSONObject r = (JSONObject) client.sendPost("add_result_for_case/" + runId + "/" + testCaseId, data);
    }

}
