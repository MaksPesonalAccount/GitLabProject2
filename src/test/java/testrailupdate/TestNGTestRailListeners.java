package testrailupdate;

import io.qameta.allure.TmsLink;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;

public class TestNGTestRailListeners implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        updateTestResult(result, TestRailStatusID.PASS, "Test passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        updateTestResult(result, TestRailStatusID.FAIL, "Test failed");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        updateTestResult(result, TestRailStatusID.UNTESTED, "Test Skipped!");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }

    //The method will be called each time after test method is passed then depends on status id we will update result
    public void updateTestResult(ITestResult result, int statusID, String comment) {
        //Get id from @TmsLink annotation
        TmsLink annotation = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TmsLink.class);
        String id = annotation.value();
        try {
            TestRailBasicApiCommand.postResultOfTestCase(statusID, 3, Integer.parseInt(id), comment);
        } catch (APIException | IOException e) {
            throw new RuntimeException(e);
        }

    }
}
