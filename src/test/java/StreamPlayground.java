import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamPlayground {

    public static void main(String[] args) {
        //sum of elements
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        int sum = numbers.stream().reduce(0, (subtotal, element) -> subtotal + element);
        System.out.println(sum);
        //Substring
        List<String> words = Arrays.asList("apple", "banana", "orange", "grapefruit");
        String substring = "an";
        var filteredList = words.stream().filter(item -> item.contains(substring)).collect(Collectors.toList());
        System.out.println(filteredList);
        //convert list string to list of integers
        List<String> strings = Arrays.asList("1", "2", "3", "4", "5");
        var integers = strings.stream().map(string -> Integer.parseInt(string)).collect(Collectors.toList());
        System.out.println(integers);
        //Sort list pf object

        List<Person> people = Arrays.asList(new Person("Alice", 25), new Person("Bob", 30), new Person("Charlie", 20));
        var sortedByAge = people.stream()
                .sorted(Comparator.comparingInt(Person::getAge))
                .collect(Collectors.toList());
        System.out.println(sortedByAge);

        //GroupBy
        List<Person> people2 = Arrays.asList(new Person("Alice", 25), new Person("Bob", 30), new Person("Charlie", 20), new Person("Ben", 30));
        var groupByList = people2.stream().collect(Collectors.groupingBy(el -> el.age));
        System.out.println(groupByList);

        //Find the maximum and minimum elements in a List of Doubles:
        List<Double> elements = Arrays.asList(1.0, 2.5, 3.2, 4.8, 5.3);
        Double min = elements.stream().mapToDouble(Double::doubleValue).min().orElseThrow();
        Double max = elements.stream().mapToDouble(Double::doubleValue).max().orElseThrow();

        //Count the number of elements in a List that satisfy a certain condition:
        List<Integer> count = Arrays.asList(1, 2, 3, 4, 5);

    }

    private static class Person {
        private String name;
        private int age;
        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
